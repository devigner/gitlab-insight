package app

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/xanzy/go-gitlab"
)

// Application defines all dependencies
type Application struct {
	Router       *mux.Router
	GitlabClient *gitlab.Client
}

// New creates the application
func New(token string, host string) *Application {
	fmt.Sprintf("Connect to gitlab %d\n", host)
	gitClient, err := gitlab.NewClient(token, gitlab.WithBaseURL(host))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	app := &Application{
		mux.NewRouter().StrictSlash(false),
		gitClient,
	}

	app.Router.HandleFunc("/", app.handler)
	app.Router.HandleFunc("/users", app.ListUsers)
	app.Router.HandleFunc("/projects", app.ListProjects)
	app.Router.HandleFunc("/pipelines", app.ListPipelines)

	app.Router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		fmt.Sprintln("Route: %s ; %s", t, route.GetName())
		return nil
	})

	app.Router.NotFoundHandler = http.HandlerFunc(app.notFound)

	return app
}

func (app Application) handler(w http.ResponseWriter, r *http.Request) {
	// di.Container().Logger.Info("Uncaugh:", r.RequestURI)
	io.WriteString(w, fmt.Sprintf("You reach page %s", r.RequestURI))
}

func (app Application) notFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	io.WriteString(w, fmt.Sprintf("404 %s", r.RequestURI))
}
