package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/xanzy/go-gitlab"
)

var projects []*gitlab.Project

func (a Application) GetProjects() []*gitlab.Project {
	if projects != nil {
		return projects
	}

	p, _, err := a.GitlabClient.Projects.ListProjects(
		&gitlab.ListProjectsOptions{
			Archived:                 gitlab.Bool(false),
			WithMergeRequestsEnabled: gitlab.Bool(true),
			Membership:               gitlab.Bool(true),
		},
	)
	if err != nil {
		log.Fatalf("Failed to load projects: %v", err)
		return projects
	}

	projects = p

	return p
}

func (a Application) ListProjects(w http.ResponseWriter, r *http.Request) {
	payload, err := json.Marshal(a.GetProjects())
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
}
