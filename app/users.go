package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/xanzy/go-gitlab"
)

func (a Application) ListUsers(w http.ResponseWriter, r *http.Request) {
	users, _, err := a.GitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{})
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	payload, err := json.Marshal(users)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
}
