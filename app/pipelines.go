package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/xanzy/go-gitlab"
)

var pipelines []*gitlab.PipelineInfo

func (a Application) GetPipelines() []*gitlab.PipelineInfo {
	if pipelines != nil {
		return pipelines
	}

	var p []*gitlab.PipelineInfo
	projects = a.GetProjects()

	for _, v := range projects {
		fmt.Printf("Get pipeline for project: %s - %d\n", v.ID, v.Name)
		pipeline, _, err := a.GitlabClient.Pipelines.ListProjectPipelines(v.ID, &gitlab.ListProjectPipelinesOptions{
			Status: gitlab.BuildState(gitlab.Created),
		})

		if err != nil {
			log.Fatalf("Failed to load projects: %v", err)
			return pipelines
		}

		p = append(p, pipeline...)
	}

	pipelines = p

	return p
}

func (a Application) ListPipelines(w http.ResponseWriter, r *http.Request) {
	payload, err := json.Marshal(a.GetPipelines())
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
}
