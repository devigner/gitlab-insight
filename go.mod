module gitlab.com/devigner/gitlab-insight

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/xanzy/go-gitlab v0.31.0
)
