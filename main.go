package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/joho/godotenv"

	"gitlab.com/devigner/gitlab-insight/app"
)

func main() {
	fmt.Println("start")
	err := godotenv.Load()
	if err != nil {
		fmt.Errorf("Error env", err.Error())
	}

	port := os.Getenv("APP_PORT")

	// users, _, err := git.Users.ListUsers(&gitlab.ListUsersOptions{})

	// t, err := template.New("foo").Parse(`{{define "T"}}Hello, {{.}}!{{end}}`)
	// err = t.ExecuteTemplate(out, "T", "<script>alert('you have been pwned')</script>")

	app := app.New(os.Getenv("GITLAB_KEY"), os.Getenv("GITLAB_HOST"))

	fmt.Println("running")
	s := &http.Server{
		Addr:           fmt.Sprintf(":%s", port),
		Handler:        app.Router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	s.ListenAndServe()
}
